from setuptools import setup

setup(
    name='assembly_scripts',
    version='0.0.1',
    description='scripts for hybrid assembly' ,
    scripts = ['scripts/combine_fasta.py',
    'scripts/get_spades_context.py',
    'scripts/get_uncovered_contigs.py',
    'scripts/phase2_assembly.sh']
)
